// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ СТАРТ
		
		// $(function(){
		// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
		// 	ua = navigator.userAgent,

		// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

		// 	scaleFix = function () {
		// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
		// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
		// 			document.addEventListener("gesturestart", gestureStart, false);
		// 		}
		// 	};
			
		// 	scaleFix();
		// });
		// var ua=navigator.userAgent.toLocaleLowerCase(),
		//  regV = /ipod|ipad|iphone/gi,
		//  result = ua.match(regV),
		//  userScale="";
		// if(!result){
		//  userScale=",user-scalable=0"
		// }
		// document.write('<meta name="viewport" id="myViewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

		// ============================================================
		//  window.onload = function () {
		// 	if(screen.width <= 617) {
		// 	    var mvp = document.getElementById('myViewport');
		// 	    mvp.setAttribute('content','width=617');
		// 	}
		// }
		// ============================================================

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ КОНЕЦ




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/




		var owl = $(".review_slider"),
		    gallerySlider = $(".gallery_slider"),
			cameraSlider = $(".camera_slider"),
			colorBox = $(".colorbox");


			if(owl.length || gallerySlider.length){
					include('plugins/owl-carousel/owl.carousel.js');
			}
			if(cameraSlider.length){
					include('plugins/camera_slider/camera.js');
					include('plugins/camera_slider/jquery.mobile.customized.min.js');
			}
			if(colorBox.length){
					include('plugins/colorbox/jquery.colorbox.js');
			}

					
					include("plugins/jquery.easing.1.3.js");



			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

		


		$(document).ready(function(){



			/* ------------------------------------------------
			OWL START
			------------------------------------------------ */

					if(owl.length){
						owl.owlCarousel({
							singleItem : true,
							items      : 1,
							loop       : true,
							smartSpeed : 1000,
					        navText    : [ '', '' ],
							nav        : true
						});
					}

					if(gallerySlider.length){
					    gallerySlider.each(function(){
					    	var $this = $(this),
					      		items = $this.data('items');

					    	$this.owlCarousel({
					    		singleItem : true,
								items : 3,
								// loop: true,
								smartSpeed:1000,
								// autoHeight:true,
					    		dots:true,
					    		nav: true,
					            navText: [ '', '' ],
					            // margin: 30,
					            responsive : items
					    	});
					    });
					}
					// <div class="owl-carousel" data-items='{  "0":{"items":1},   "480":{"items":2},   "991":{"items":3}  }'></div>

			/* ------------------------------------------------
			OWL END
			------------------------------------------------ */




			/* ------------------------------------------------
			CAMERA SLIDER START
			------------------------------------------------ */

					if(cameraSlider.length){
						cameraSlider.camera({
							pagination: true,
							pauseOnClick: false,
							loader: 'none',
							navigation: false,
							slideOn: 'next',
							playPause: false
						});
					}

			/* ------------------------------------------------
			CAMERA SLIDER END
			------------------------------------------------ */


			


		});

		
		$(window).load(function(){

			
			/* ------------------------------------------------
			COLORBOX START
			------------------------------------------------ */

					if(colorBox.length){
						colorBox.colorbox({
							rel:'gal2', 
							transition:"fade"

						});
					}

		    /* ------------------------------------------------
			COLORBOX END
			------------------------------------------------ */


		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
